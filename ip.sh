#!/bin/sh
# Created by Clem Morton, April 2013. http://cmorton.com
# Check README.md for more help!
#
ip1=$(wget -qO- http://example.com/ip.php)
ip2=$(wget -qO- http://example.com/ip.php) #Must be on a separate server ip.
if [ $ip1 != $ip2 ]
	then
	echo $ip1 ":" $ip2 ": Tunnel Active"> ~/tmp/ip.tmp && cat ~/tmp/ip.tmp
	else
	echo $ip1 ":" $ip2 > ~/tmp/ip.tmp && cat ~/tmp/ip.tmp
fi
